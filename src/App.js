import React, { useState } from "react";
import './App.css';
import Header from "./pages/Header";
import Home from './pages/Home'
import About from './pages/About'
import Mission from './pages/Mission'
import Subscribe from './pages/Subscribe';
import Footer from './pages/Footer';

function App() {
  return (
    <div className="App">

      <Header />

      <Home />
      <hr />

      <About />
      <hr />

      <Mission />
      <hr />

      <Subscribe />

      <Footer />
    </div>
  );
}

export default App;
