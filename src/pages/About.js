import React from 'react'

const About = () => {

  return (
    <div className='grid-two-columns section' id='about'>

      <div>
        <h1 className='about-title'>About Us</h1>
        <div className='grid-three-columns'>
          <div>
            <h2>200+</h2>
            <p>Galleries</p>
          </div>
          <div>
            <h2>365</h2>
            <p>Collections</p>
          </div>
          <div>
            <h2>130+</h2>
            <p>Artists</p>
          </div>
        </div>
        <p className='about-description'>Like how blockchain democratizes finance like never before, APENFT, by turning top artists and art pieces into NFTs,<br/><br/>
          not only upgrades the way artworks are hosted, but also transforms them from being elite-exclusive items to something
          that truly belongs to the people and mirrors their aspirations. APENFT is the art for everyone.</p>
        <button className='read-more-button'>Read More →</button>
      </div>

      <img src='https://picsum.photos/600/379' className='about-image' />


    </div>
  )
}

export default About