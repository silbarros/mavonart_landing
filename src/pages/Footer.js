import React from 'react';
import '../App.css';

const Footer = () => {
  return (
      <footer className='footer'>
        <div className="footer-wrapper">
          <div className='column-left'>
            <h2>About Us</h2>
            <p>Who are we?</p>
            <p>What we do?</p>
          </div>

          <div>
            <h2>Acquisitions</h2>
            <p>Who are we?</p>
            <p>What we do?</p>
          </div>

          <div className='column-right'>
            <h2>Exhibitions</h2>
            <p>Who are we?</p>
            <p>What we do?</p>
          </div>
        </div>

        <div className='link'>
          <a href="#" target="_blank"><i className="fab fa-instagram"></i></a>
          <a href="#" target="_blank"><i className="fab fa-facebook"></i></a>
          <a href="#" target="_blank"><i className="fab fa-linkedin"></i></a>
          <a href="mailto: example@mavon.art" target="_blank"><i className="far fa-envelope"></i></a>
        </div>
      </footer>
  )
};

export default Footer;
