import React, { useState } from "react";
import '../App.css';
import SubscribeForm from './SubscribeForm'

function App() {
  return (
    <div className='section' id='subscribe'>
      <div className='subscribe-content'>
        <h1 className='subscribe-title'>Subscribe</h1>
        <div class='subscribe-content-text'>
          <p>Subscribe to our newsletter and receive the last news about digital art!</p>
          <SubscribeForm/>
        </div>
      </div>
    </div>
  );
}

export default App;
